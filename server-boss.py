# dict[i] = [ip, place, input_or_output, port, lenght]

import json
import re
import socket
from datetime import datetime
import kivy
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
import threading

import _thread

#import update_template.py

PORT = 800

"""
def gen_response():
    return 'this_is_the_return_from_the_server'




def labels_replace(not_labels, data, temp):
    return re.sub("labels: \[[^]]*\]%s" % (not_labels), "labels " + ":" + data[data.find('(') + 1:data.find(')')], temp)


def data_replace(not_data, data, temp):
    return re.sub("data: \[[^]]*\]%s" % (not_data), "data " + ":" + data[data.find('(') + 1:data.find(')')], temp)

def time_replace(not_time,time,temp):

        return re.sub("Last update:" % (not_time), "Last update:" +datetime.datetime.now(), temp)


def update_temp(list_data, name, client_addr, dict_block, name_dict1, name_dict2, country_dict, ip_dict, app_dict,
                port_dict):
    dict_block = block(dict_block, list_data, name, client_addr[0])
    for packet in list_data:
        if name not in name_dict1.keys():
            name_dict1[name] = 0
            name_dict2[name] = 0
        elif name in name_dict1.keys() and packet['input_or_output'] == "output":
            name_dict1[name] += 1
        elif name not in name_dict1.keys() and packet['input_or_output'] == "input":
            name_dict2[name] += 1
    for country in list_data:
        if country['place'] != "null" or country['place'] !="undefined":
            if country['place'] in country_dict.keys():
                country_dict[country['place']] += 1
            else:
                country_dict[country['place']] = 1
    for i in list_data:
        ip_dict[i['ip']] = 0
        for f in list_data:
            if i['ip'] == f['ip']:
                ip_dict[i['ip']] += 1
    for i in list_data:
        if i['app']!="unknown":
            app_dict[i['app']] = 0
        for f in list_data:
            if i['app']!="unknown":
                if i['app'] == f['app']:
                    app_dict[i['app']] += 1
    for i in list_data:
        port_dict[i['port']] = 0
        for f in list_data:
            if i['port'] == f['port']:
                port_dict[i['port']] += 1
    print (port_dict)
    # open the temp file and update him with replace
    new_file = open("temp2.html", "w")
    file = open("temp.html", "r").read()
    # labels_replace('start1',name_arr,temp)
    # data_replace
    file = labels_replace('\/\*start1\*\/', str(name_dict1.keys()), file)
    file = labels_replace('\/\*start2\*\/', str(name_dict1.keys()), file)
    file = labels_replace('\/\*start3\*\/', str(name_dict2.keys()), file)
    file = labels_replace('\/\*start4\*\/', str(name_dict2.keys()), file)
    file = labels_replace('\/\*start5\*\/', str(country_dict.keys()), file)
    file = data_replace('\/\*start6\*\/', str(country_dict.values()), file)
    file = labels_replace('\/\*start7\*\/', str(ip_dict.keys()), file)
    file = data_replace('\/\*start8\*\/', str(ip_dict.values()), file)
    file = labels_replace('\/\*start9\*\/', str(app_dict.keys()), file)
    file = data_replace('\/\*start10\*\/', str(app_dict.values()), file)
    file = labels_replace('\/\*start11\*\/', str(port_dict.keys()), file)
    file = data_replace('\/\*start12\*\/', str(port_dict.values()), file)
    file = re.sub("\/\*start13\*\/",str(dict_block)[str(dict_block).find('(') + 1:str(dict_block).find(')') - 1], file)
    file = data_replace('\/\*start0\*\/', str(port_dict.values()), file)

    new_file.write(file)
    new_file.close()


:returns dict with mane of block site and the name and ip of the pc that try to get into
"""

def block(dict_block, list_data, name, addr):
    file = open("settings.dat", "r").read()
    block_sites = file[file.find('start') + 6:file.find('end') - 1].split('\n')
    for i in list_data:
        for block1 in block_sites:
            if i['ip'] in block1:
                dict_block[block1.split(':')[0]] = str(name) + ":" + str(addr)
    return dict_block


#replace labels in the html temp
def labels_replace(not_labels, data, temp):
    return re.sub("labels: \[[^]]*\]%s" % (not_labels), "labels " + ":" + "["+data+"]", temp)
    print(data)


#replace data in the html temp
def data_replace(not_data, data, temp):
    print(data)
    return re.sub("data: \[[^]]*\]%s" % (not_data), "data " + ":" + "["+data+"]", temp)



"""get the json packet from the clinet that contains all the info taht i need than it take
that info and insert to the right lists or dicts and replacer them with the old info in the html temp"""
def update():
    # read content

    packets = ""
    incoming_traffic_per_agent = {}
    outcoming_traffic_per_agent = {}
    traffic_per_country = {}
    traffic_per_IP = {}
    traffic_per_app = {}
    traffic_per_port = {}
    with open(r"logfile.txt","r") as logfile:
        # sort packets
        packets = logfile.read().split("\n")
        del packets[-1]
        for pack1 in packets:
            abc = pack1.replace("'", "\"")
            #print(abc)
            pack = json.loads(abc)
            #print(pack)
            # traffic per agent
            if pack["input_or_output"] == "input":
                # incoming_traffic_per_agent
                if pack["name"] in incoming_traffic_per_agent.keys():
                    incoming_traffic_per_agent[pack["name"]] += pack["size"]
                else:
                    incoming_traffic_per_agent[pack["name"]] = pack["size"]
            else:
                # outcoming_traffic_per_agent
                if pack["name"] in outcoming_traffic_per_agent.keys():
                    outcoming_traffic_per_agent[pack["name"]] += pack["size"]
                else:
                    outcoming_traffic_per_agent[pack["name"]] = pack["size"]

            # traffic per country
            if pack["country"] in traffic_per_country.keys():
                traffic_per_country[pack["country"]] += pack["size"]
            else:
                traffic_per_country[pack["country"]] = pack["size"]

            # traffic per ip
            if pack["Ip"] in traffic_per_IP.keys():
                traffic_per_IP[pack["Ip"]] += pack["size"]
            else:
                traffic_per_IP[pack["Ip"]] = pack["size"]

            # traffic per app
            if pack["app"] in traffic_per_app.keys():
                traffic_per_app[pack["app"]] += pack["size"]
            else:
                traffic_per_app[pack["app"]] = pack["size"]

            # traffic per port
            if pack["port"] in traffic_per_port.keys():
                traffic_per_port[pack["port"]] += pack["size"]
            else:
                traffic_per_port[pack["port"]] = pack["size"]
    #print(incoming_traffic_per_agent)
    file = open("temp.html", "r").read()
    # enter new time stamp
    file= file.replace("%%TIMESTAMP%%", str(datetime.now()))

    # update agents (in)
    names = ''.join(str(set(incoming_traffic_per_agent.keys())).replace("{", "").replace("}", ""))
    file = labels_replace('\/\*start1\*\/',names, file)

    # update bytes (agents) (in)
    sizes = ''.join(str(set(incoming_traffic_per_agent.values())).replace("{", "").replace("}", ""))
    file = labels_replace('\/\*start2\*\/', sizes, file)
    #print(sizes)
    # update agents (out)
    names1 = ''.join(str(set(outcoming_traffic_per_agent.keys())).replace("{", "").replace("}", ""))
    file = labels_replace('\/\*start3\*\/', names1, file)

    # update bytes (agents) (out)

    sizes1 = ''.join(str(set(outcoming_traffic_per_agent.values())).replace("{", "").replace("}", ""))
    file = labels_replace('\/\*start4\*\/', sizes1, file)
    # update country

    country = ''.join(str(set(traffic_per_country.keys())).replace("{", "").replace("}", ""))
    file = labels_replace('\/\*start5\*\/', country, file)
    # update bytes (country)

    sizes2 = ''.join(str(set(traffic_per_country.values())).replace("{", "").replace("}", ""))
    file = data_replace('\/\*start6\*\/', sizes2, file)

    # update ip
    ip = ''.join(str(set(traffic_per_IP.keys())).replace("{", "").replace("}", ""))
    file = labels_replace('\/\*start7\*\/',ip, file)
    # update bytes (ip)
    sizes3 = ''.join(str(set(traffic_per_IP.values())).replace("{", "").replace("}", ""))
    file = data_replace('\/\*start8\*\/',sizes3, file)

    # update app]

    app = ''.join(str(set(traffic_per_app.keys())).replace("{", "").replace("}", ""))
    file = labels_replace('\/\*start9\*\/', app, file)
    # update bytes (app)

    sizes4 = ''.join(str(set(traffic_per_app.values())).replace("{", "").replace("}", ""))
    file = data_replace('\/\*start10\*\/', sizes4, file)

    # update port
    port = ''.join(str(set(traffic_per_port.keys())).replace("{", "").replace("}", ""))
    file = labels_replace('\/\*start11\*\/', port, file)
    # update bytes (port)
    sizes5 = ''.join(str(set(traffic_per_port.values())).replace("{", "").replace("}", ""))
    file = data_replace('\/\*start12\*\/', sizes5, file)

    f = open(r"update_temp.html", "w")
    f.write(file)

print(socket.gethostbyname_ex(socket.gethostname())[2][len(socket.gethostbyname_ex(socket.gethostname())[2])-1])
HOST =socket.gethostbyname_ex(socket.gethostname())[2][len(socket.gethostbyname_ex(socket.gethostname())[2])-1]

def main():
    """country_dict = {}
    ip_dict = {}
    app_dict = {}
    port_dict = {}
    name_dict1 = {}
    name_dict2 = {}
    dict_block = {}"""
    HOST =socket.gethostbyname_ex(socket.gethostname())[2][len(socket.gethostbyname_ex(socket.gethostname())[2])-1]
    print(HOST)
#start the udp server
    listen_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    listen_socket.bind((HOST, PORT))
    print(HOST,PORT)
    """wire into the file what sites are not allowed"""
    file = open("settings.dat", "w")
    file.write("start\n")
    file.close()
    not_allowed = ""

    """while not_allowed != 'end':
        not_allowed = input("enter not allowed sites (name:ip).\nTo exsit enter end\n")
        file.write(not_allowed + '\n')
    file.close()"""

    print("whiting for connection")
    while True:

        client_msg, client_addr = listen_socket.recvfrom(64000)

        time=listen_socket.recv(64000)
        HOST =[l for l in ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1], [[(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0]
        print(time)
        try:
            listen_socket.sendto(b"connected " + client_msg + b"\n", client_addr)
            name, data = client_msg.decode("UTF8").split('-->')
            file = open("settings.dat", "r").read()
            listen_socket.sendto(file.encode(),client_addr)
            with open(r"logfile.txt", "a") as f:
                f.write(str(data)+"\n")
            #update_temp(data, name, client_addr, dict_block, name_dict1, name_dict2, country_dict, ip_dict, app_dict, port_dict)
            print(name + "-->", data)

            listen_socket.sendto(b"open", client_addr)
            file = open("settings.dat", "r").read()
            listen_socket.sendto(file.encode(),client_addr)
        except ValueError:
            listen_socket.sendto(b"connected " + client_msg + b"\n", client_addr)
            file = open("settings.dat", "r").read()
            listen_socket.sendto(file.encode(),client_addr)
            listen_socket.sendto(b"connected " + client_msg + b"\n", client_addr)

    listen_socket.close()

# the gui class contains the settings of the gui
class MyGrid(GridLayout):
    def __init__(self, **kwargs):
        #start new thread for the server for runing with the gui
        _thread.start_new_thread(main,())
        super(MyGrid, self).__init__(**kwargs)
        self.cols = 1

        self.inside = GridLayout()
        self.inside.cols = 2

        self.inside.add_widget(Label(text="enter block site"))
        self.block = TextInput(multiline=False)
        self.inside.add_widget(self.block)


        #self.inside.add_widget(Label(text="enter unblock site(enter one site)"))
        #self.unblock = TextInput(multiline=False)
        #self.inside.add_widget(self.unblock)


        self.add_widget(self.inside)

        self.submit = Button(text="block", font_size=40)
        self.submit.bind(on_press=self.pressed)
        self.add_widget(self.submit)

        self.submit = Button(text="update", font_size=40)
        self.submit.bind(on_press=self.pressed2)
        self.add_widget(self.submit)

        self.submit = Button(text="unblock", font_size=40)
        self.submit.bind(on_press=self.pressed3)
        self.add_widget(self.submit)
        self.submit = Button(text="server ip is:  "+HOST, font_size=40)

        self.add_widget(self.submit)

#functions for the buttons
    def pressed(self, instance):
        block = self.block.text
        file = open("settings.dat", "a")
        file.write(block + '\n')
        file.close()
        self.block.text = ""

    def pressed2(self, instance):
        update()

    def pressed3(self, instance):

        unblock = self.block.text
        print(unblock)
        f = open("settings.dat","r")
        lines = f.readlines()
        f.close()
        f = open("settings.dat","w")
        for line in lines:
          if unblock not in line:
            f.write(line)
        self.block.text = ""
class MyApp(App):
    def build(self):
        return MyGrid()


if __name__ == "__main__":
    MyApp().run()